package db.persistence;


import db.pojo.Wxuserinfor;

import java.util.List;

public interface WxuserinforMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Wxuserinfor record);

    int insertSelective(Wxuserinfor record);

    Wxuserinfor selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Wxuserinfor record);

    int updateByPrimaryKey(Wxuserinfor record);

    List<Wxuserinfor> selectall();
}