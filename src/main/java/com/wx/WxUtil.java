package com.wx;

import weixin.mp.api.WxMpInMemoryConfigStorage;
import weixin.mp.api.WxMpService;
import weixin.mp.api.WxMpServiceImpl;


public class WxUtil {
	public static WxMpService wxMpService;
	
	public static WxMpService getInstance() {
		if (wxMpService == null) {
			WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage(); 
			
			config.setAppId(IceAllianceConstant.APPID); // 设置微信公众号的appid
			config.setSecret(IceAllianceConstant.SECRET); // 设置微信公众号的app  corpSecret
			config.setToken(IceAllianceConstant.TOKEN); // 设置微信公众号的token
			config.setAesKey(IceAllianceConstant.AESKEY); // 设置微信公众号的EncodingAESKey

			config.setOauth2redirectUri("http://weiweiweixin.ngrok.io/weixin/openid.jsp");

			wxMpService = new WxMpServiceImpl();
			wxMpService.setWxMpConfigStorage(config);
            System.out.println( config.toString());
            System.out.println( "getinstance");


		}

		return wxMpService;

	}

}
